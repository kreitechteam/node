module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "no-underscore-dangle": 0,
        "no-shadow": 0,
        "no-console": 0,
        "func-names": 0,
        "no-param-reassign": 0,
        "no-useless-escape": 0,
        "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
    },
    "env": {
        "node": true,
        "mocha": true
    }
};