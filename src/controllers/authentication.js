import jwt from 'jwt-simple';
import crypto from 'crypto';
import smtpTransport from '../services/mail';
import User from '../models/user';
import config from '../config';

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp, role: user.role }, config.secret);
}

export const signin = (req, res) => {
  if (req.user.activated) {
    res.send({
      token: tokenForUser(req.user),
      email: req.user.email,
      name: req.user.fullName,
      role: req.user.role,
      id: req.user._id,
    });
  } else {
    res.status(400).send({
      error: 'User has not been activated',
    });
  }
};

/* eslint-disable */
export const signinError = (err, req, res, next) => res.status(400).send({ error: 'Username or password is incorrect' });
/* eslint-enable */

export const activateUser = (req, res) => {
  const { token } = req.body;
  return User.findOneAndUpdate({ activateUserToken: token, activated: false },
    { activated: true, activateUserToken: undefined }).then((user) => {
    if (!user) {
      return res.status(400).send({ error: 'User not found' });
    }
    return res.sendStatus(200);
  });
};

export const passwordReset = (req, res) => {
  const { token, password } = req.body;
  User.updatePasswordWithRecoveryToken(token, password).then((user) => {
    user.id = user._id;
    res.send({
      token: tokenForUser(user),
      email: user.email,
      name: user.fullName,
      role: user.role,
      id: user._id,
    });
  }).catch((error) => {
    res.status(400).send({ error: error.message });
  });
};

export const passwordResetSend = (req, res) => {
  const { email } = req.body;
  User.generatePasswordRecoveryToken(email).then((token) => {
    const mailOptions = {
      to: email,
      from: 'passwordreset@demo.com',
      subject: 'Node.js Password Reset',
      text: `${'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n'
        + 'Please click on the following link, or paste this into your browser to complete the process:\n\n'
        + 'https://project-base.kreitech.io/forgot/'}${token}\n\n`
        + 'If you did not request this, please ignore this email and your password will remain unchanged.\n',
    };
    return smtpTransport.sendMail(mailOptions);
  }).then(() => {
    res.sendStatus(200);
  }).catch((error) => {
    console.log(error.message);
    res.status(400).send({ error: error.message });
  });
};

export const signup = (req, res) => {
  const { name, password } = req.body;
  let { email } = req.body;

  if (!email || !password) {
    return res.status(422).send({ error: 'password & email required' });
  }

  email = email.toLowerCase();

  if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
    return res.status(400).send({ error: 'Invalid email' });
  }

  return User.findOne({ email }, (err, existingUser) => {
    if (err) {
      return res.status(400).send(err);
    }

    if (existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    return crypto.randomBytes(20, (err, buf) => {
      const activateUserToken = buf.toString('hex');
      const user = new User({
        fullName: name, email, password, activateUserToken,
      });

      return user.save().then(() => {
        const mailOptions = {
          to: email,
          from: 'passwordreset@demo.com',
          subject: 'Account activation',
          text: `${'Please click in the following link in order to activate your account.\n\n'
            + 'https://project-base.kreitech.io/activate/'}${activateUserToken}\n\n`,
        };
        return smtpTransport.sendMail(mailOptions);
      }).then(() => {
        res.sendStatus(200);
      }).catch((error) => {
        console.log(error.message);
        res.status(400).send({ error: error.message });
      });
    });
  });
};
