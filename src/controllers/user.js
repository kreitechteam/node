import paginate from 'express-paginate';
import User from '../models/user';


export const update = (req, res) => {
  if ((req.user.role !== 'admin') && (req.user.id !== req.params.id)) {
    return res.sendStatus(401);
  }

  if (!req.body) {
    return res.status(400).send({
      error: 'Empty body',
    });
  }
  return User.findById(req.params.id).then((user) => {
    if (req.body.fullName) user.fullName = req.body.fullName;
    if (req.body.password) user.password = req.body.password;
    return user.save();
  }).then((user) => {
    res.send({
      id: user._id, fullName: user.fullName, email: user.email, role: user.role,
    });
  }).catch((error) => {
    res.status(500).send(error);
  });
};

export const getOne = (req, res) => {
  if ((req.user.role !== 'admin') && (req.user.id !== req.params.id)) {
    return res.sendStatus(401);
  }

  return User.findById(req.params.id).select('role email fullName').then((user) => {
    res.send({ user });
  }).catch((error) => {
    res.status(500).send(error);
  });
};

export const get = (req, res) => {
  User.find().select('role email fullName').limit(req.query.limit).skip(req.skip)
    .then((users) => {
      User.countDocuments({}).then((count) => {
        const pageCount = Math.ceil(count / req.query.limit);
        res.send({
          has_more: paginate.hasNextPages(req)(pageCount),
          data: users,
        });
      });
    })
    .catch((error) => {
      res.status(500).send(error);
    });
};
