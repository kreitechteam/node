import chai, { expect, request } from 'chai';
import chaiHttp from 'chai-http';
import sinon from 'sinon';
import app from '../..';
import User from '../../models/user';
import { users, populateUsers } from '../seed';
import smtpTransport from '../../services/mail';

const baseUrl = '/api/v1';
chai.use(chaiHttp);
const sendMailStub = sinon.stub(smtpTransport, 'sendMail');
const email = 'email@email.com';

before(populateUsers);

describe('POST /signup', () => {
  it('should create a new user', (done) => {
    request(app)
      .post(`${baseUrl}/signup`)
      .send({ name: 'name', password: 'password', email })
      .end((err, res) => {
        expect(res).to.have.status(200);
        User.find({ email }).then((users) => {
          expect(users.length).to.equal(1);
          done();
        }).catch(e => done(e));
      });
  });

  it('should send email', () => {
    expect(sendMailStub.getCalls().length).to.equal(1);
  });

  it('should not signup user with invalid email', (done) => {
    const email = 'email';
    request(app)
      .post(`${baseUrl}/signup`)
      .send({ name: 'name', password: 'password', email })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body).to.eql({ error: 'Invalid email' });
        User.find({ email }).then((users) => {
          expect(users.length).to.equal(0);
          done();
        }).catch(e => done(e));
      });
  });

  it('should not signup user without email', (done) => {
    const email = 'email';
    request(app)
      .post(`${baseUrl}/signup`)
      .send({ name: 'name', password: 'password' })
      .end((err, res) => {
        expect(res).to.have.status(422);
        expect(res.body).to.eql({ error: 'password & email required' });
        User.find({ email }).then((users) => {
          expect(users.length).to.equal(0);
          done();
        }).catch(e => done(e));
      });
  });
});

describe('POST /auth/activate', () => {
  it('should activate user', (done) => {
    request(app)
      .post(`${baseUrl}/auth/activate`)
      .send({ token: users[0].activateUserToken })
      .end((err, res) => {
        expect(res).to.have.status(200);
        User.findOne({ email: users[0].email }).then((user) => {
          expect(user.activated).to.equal(true);
          expect(user.activateUserToken).to.equal(null);
          done();
        }).catch(e => done(e));
      });
  });

  it('should send status 400 when user not found', (done) => {
    request(app)
      .post(`${baseUrl}/auth/activate`)
      .send({ token: 'unexisting token' })
      .end((err, res) => {
        expect(res).to.have.status(400);
        done();
      });
  });
});

describe('POST reset password', () => {
  beforeEach(() => {
    sendMailStub.resetHistory();
  });

  it('should send email with token', (done) => {
    request(app)
      .post(`${baseUrl}/auth/get_reset_token`)
      .send({ email: users[0].email })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(sendMailStub.getCalls().length).to.equal(1);
        done();
      });
  });

  it('should reset password with token', (done) => {
    User.findOne({ email: users[0].email }).then((user) => {
      const token = user.resetPasswordToken;
      request(app)
        .post(`${baseUrl}/auth/use_reset_token`)
        .send({ token, password: 'newpassword' })
        .end((err, res) => {
          expect(res).to.have.status(200);
          User.findOne({ email: users[0].email }).then((userUpdated) => {
            expect(user.password).to.not.equal(userUpdated.password);
            done();
          });
        });
    });
  });
});

describe('POST sign in', () => {
  it('should send token', (done) => {
    const { email, password } = users[2];
    request(app)
      .post(`${baseUrl}/signin`)
      .send({ email, password })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('token');
        done();
      });
  });
});
