import chai, { expect, request } from 'chai';
import chaiHttp from 'chai-http';
import app from '../..';
import User from '../../models/user';
import { users, populateUsers } from '../seed';

const baseUrl = '/api/v1';
chai.use(chaiHttp);

before(populateUsers);
const user = users[2];
const admin = users[3];
const updateObject = { fullName: 'new name', password: 'new password' };

describe('Update user', () => {
  it('should update user', (done) => {
    request(app)
      .put(`${baseUrl}/users/${user._id}`)
      .set('Authorization', user.token)
      .send(updateObject)
      .end((err, res) => {
        expect(res).to.have.status(200);
        User.findOne({ email: user.email }).then((user) => {
          expect(user.fullName).to.equal(updateObject.fullName);
          done();
        }).catch(e => done(e));
      });
  });

  it('should not allow user to update other user profile', (done) => {
    request(app)
      .put(`${baseUrl}/users/${admin._id}`)
      .set('Authorization', user.token)
      .send(updateObject)
      .end((err, res) => {
        expect(res).to.have.status(401);
        done();
      });
  });
});

describe('GET /users', () => {
  it('should require admin to get users', (done) => {
    request(app)
      .get(`${baseUrl}/users`)
      .set('Authorization', user.token)
      .end((err, res) => {
        expect(res).to.have.status(401);
        done();
      });
  });

  it('should get users if admin', (done) => {
    request(app)
      .get(`${baseUrl}/users`)
      .set('Authorization', admin.token)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.data.length).to.eql(5);
        expect(res.body.has_more).to.equal(false);
        done();
      });
  });
});

describe('GET /user', () => {
  it('should get user', (done) => {
    request(app)
      .get(`${baseUrl}/users/${user._id}`)
      .set('Authorization', user.token)
      .end((err, res) => {
        const { _id, email, role } = user;
        expect(res).to.have.status(200);
        expect(res.body.user).to.eql({
          fullName: updateObject.fullName, email, role, _id: _id.toHexString(),
        });
        done();
      });
  });

  it('should not allow user to get other user', (done) => {
    request(app)
      .get(`${baseUrl}/users/${admin._id}`)
      .set('Authorization', user.token)
      .end((err, res) => {
        expect(res).to.have.status(401);
        done();
      });
  });
});
