import { expect } from 'chai';
import User from '../../models/user';
import { populateUsers } from '../seed';

before(populateUsers);

it('should create new user using facebook', (done) => {
  const accessToken = 'access token';
  const refreshToken = 'refresh token';
  const email = 'email@facebook.com';
  const profile = { displayName: 'name', emails: [{ value: email }], id: 'profile id' };
  User.upsertFbUser(accessToken, refreshToken, profile, () => {
    User.findOne({ email }).select('+facebookProvider').then((user) => {
      expect(user.fullName).to.equal(profile.displayName);
      expect(user.activated).to.equal(true);
      expect(user.facebookProvider.id).to.equal(profile.id);
      expect(user.facebookProvider.token).to.equal(accessToken);
      User.deleteOne({ email }).then(() => {
        done();
      });
    });
  });
});

it('should create new user using google', (done) => {
  const accessToken = 'access token';
  const refreshToken = 'refresh token';
  const email = 'email@google.com';
  const profile = { displayName: 'name', emails: [{ value: email }], id: 'profile id' };
  User.upsertGoogleUser(accessToken, refreshToken, profile, () => {
    User.findOne({ email }).select('+googleProvider').then((user) => {
      expect(user.fullName).to.equal(profile.displayName);
      expect(user.activated).to.equal(true);
      expect(user.googleProvider.id).to.equal(profile.id);
      expect(user.googleProvider.token).to.equal(accessToken);
      User.deleteOne({ email }).then(() => {
        done();
      });
    });
  });
});
