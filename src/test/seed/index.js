import { ObjectID } from 'mongodb';
import jwt from 'jwt-simple';
import config from '../../config';
import User from '../../models/user';

export const users = [{
  _id: new ObjectID(),
  email: 'one@example.com',
  password: 'password',
  role: 'admin',
  activateUserToken: 'activate user token 1',
}, {
  _id: new ObjectID(),
  email: 'two@example.com',
  password: 'password',
  role: 'user',
  activateUserToken: 'activate user token 2',
}, {
  _id: new ObjectID(),
  email: 'three@example.com',
  password: 'password',
  role: 'user',
  activated: true,
}, {
  _id: new ObjectID(),
  email: 'four@example.com',
  password: 'password',
  role: 'admin',
  activated: true,
}];

users.forEach((user) => {
  const timestamp = new Date().getTime();
  user.token = jwt.encode({ sub: user._id, iat: timestamp, role: user.role }, config.secret);
});

export const populateUsers = (done) => {
  User.remove({}).then(() => {
    const userOne = new User(users[0]).save();
    const userTwo = new User(users[1]).save();
    const userThree = new User(users[2]).save();
    const userFour = new User(users[3]).save();

    return Promise.all([userOne, userTwo, userThree, userFour]);
  }).then(() => done());
};
