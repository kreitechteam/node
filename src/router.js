import express from 'express';
import {
  signin, signup, passwordReset, passwordResetSend, activateUser, signinError,
} from './controllers/authentication';
import { get, update, getOne } from './controllers/user';
import {
  requireAuth, requireSignin, requireFacebook, requireGoogle,
} from './services/passport';
import { admin } from './services/roles';

const router = express.Router();

router.post('/signin', requireSignin, signin, signinError);
router.post('/signup', signup);
router.post('/auth/facebook', requireFacebook, signin);
router.post('/auth/google', requireGoogle, signin);
router.get('/users', requireAuth, admin, get);
router.put('/users/:id', requireAuth, update);
router.get('/users/:id', requireAuth, getOne);

router.post('/auth/activate', activateUser);
router.post('/auth/use_reset_token', passwordReset);
router.post('/auth/get_reset_token', passwordResetSend);

module.exports = router;
