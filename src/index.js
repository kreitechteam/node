import express from 'express';
import bodyParser from 'body-parser';
import morgan from 'morgan';
import mongoose from 'mongoose';
import cors from 'cors';
import paginate from 'express-paginate';
import rateLimit from 'express-rate-limit';
import helmet from 'helmet';
import routes from './router';
import config from './config';

const app = express();

let mongo;
if (process.env.NODE_ENV !== 'test') {
  mongo = config.mongoUrl;
} else {
  mongo = config.mongoUrlTest;
}

mongoose.connect(mongo, { useNewUrlParser: true }).catch(() => {
  process.exit(1);
});

const apiLimiter = rateLimit({
  windowMs: 10 * 60 * 1000, // 10 minutes
  max: 500,
});

// This is to tell express that it is behind a proxy
// In case you use a reverse proxy like nginx in production
app.enable('trust proxy');

app.use('/api/', apiLimiter);
app.use(helmet());

if (process.env.NODE_ENV !== 'test') {
  app.use(morgan('combined'));
}

app.use(cors());
app.use(bodyParser.json({ type: '*/*' }));
app.use(paginate.middleware(10, 50));
app.use('/api/v1/', routes);

const port = process.env.PORT || 3090;
app.listen(port, () => {
  console.log('Server listening on:', port);
});

export default app;
