import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt-nodejs';
// todo use bcrypt random string if exists
import crypto from 'crypto';

const userSchema = new Schema({
  fullName: String,
  email: {
    type: String,
    unique: true,
    lowercase: true,
    /* validate: {
      validator: email => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email),
      message: 'invalid email format',
    }, */
  },
  activated: {
    type: Boolean,
    default: false,
  },
  activateUserToken: String,
  password: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,
  role: {
    type: String,
    enum: ['user', 'admin'],
    default: 'user',
  },
  googleProvider: {
    type: {
      id: String,
      token: String,
    },
    select: false,
  },
  facebookProvider: {
    type: {
      id: String,
      token: String,
    },
    select: false,
  },
});

// If user logged with oauth, password field appears in db
// but it's not used to login

userSchema.pre('save', function (next) {
  const user = this;

  if (!user.isModified('password')) {
    return next();
  }

  return bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }

    return bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return next(err); }

      user.password = hash;
      return next();
    });
  });
});

userSchema.methods.comparePasswords = function (candidatePassword, callback) {
  return bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) { return callback(err); }
    return callback(null, isMatch);
  });
};

userSchema.statics.upsertFbUser = function (accessToken, refreshToken, profile, callback) {
  const That = this;
  return this.findOne({
    'facebookProvider.id': profile.id,
  }, (err, user) => {
    // no user was found, lets create a new one
    if (!user) {
      const newUser = new That({
        activated: true,
        fullName: profile.displayName,
        email: profile.emails[0].value,
        facebookProvider: {
          id: profile.id,
          token: accessToken,
        },
      });

      return newUser.save((error, savedUser) => {
        if (error) {
          console.log(error);
        }
        return callback(error, savedUser);
      });
    }
    return callback(err, user);
  });
};

userSchema.statics.upsertGoogleUser = function (accessToken, refreshToken, profile, cb) {
  const That = this;
  return this.findOne({
    'googleProvider.id': profile.id,
  }, (err, user) => {
    // no user was found, lets create a new one
    if (!user) {
      const newUser = new That({
        activated: true,
        fullName: profile.displayName,
        email: profile.emails[0].value,
        googleProvider: {
          id: profile.id,
          token: accessToken,
        },
      });
      return newUser.save((error, savedUser) => {
        if (error) {
          console.log(error);
        }
        return cb(error, savedUser);
      });
    }
    return cb(err, user);
  });
};

userSchema.statics.updatePasswordWithRecoveryToken = function (resetPasswordToken, password) {
  const That = this;
  return new Promise((resolve, reject) => {
    That.findOne({ resetPasswordToken, resetPasswordExpires: { $gt: Date.now() } }).then(
      (user) => {
        if (!user) {
          reject(new Error('User not found or token expired'));
        } else {
          // expire token
          user.resetPasswordExpires = Date.now();
          user.password = password;
          user.activated = true;
          user.save().then(() => {
            resolve(user);
          });
        }
      },
    );
  });
};

userSchema.statics.generatePasswordRecoveryToken = function (email) {
  const That = this;
  return new Promise((resolve, reject) => {
    crypto.randomBytes(20, (err, buf) => {
      const token = buf.toString('hex');
      return That.findOne({ email }).then((user) => {
        if (!user) {
          return reject(new Error('User not found'));
        }
        user.resetPasswordToken = token;
        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour
        return user.save().then(() => resolve(token));
      });
    });
  });
};

const ModelClass = mongoose.model('user', userSchema);

module.exports = ModelClass;
