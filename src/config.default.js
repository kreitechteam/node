const mongo = process.env.MONGODB || 'localhost';
const config = {
  secret: '',
  googleAuth: {
    clientID: '',
    clientSecret: '',
  },
  facebookAuth: {
    clientID: '',
    clientSecret: '',
  },
  mail: {
    user: '',
    pass: '',
  },
  mongoUrl: `mongodb://${mongo}:27017/node_base`,
  mongoUrlTest: `mongodb://${mongo}:27017/node_base_testing`,
};

export default config;
