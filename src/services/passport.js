import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import LocalStrategy from 'passport-local';
import { Strategy as GoogleTokenStrategy } from 'passport-google-token';
import FacebookTokenStrategy from 'passport-facebook-token';
import config from '../config';
import User from '../models/user';

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.secret,
};

const localLogin = new LocalStrategy({ usernameField: 'email' }, ((email, password, done) => {
  const emailToLower = email.toLowerCase();
  User.findOne({ email: emailToLower }, (err, user) => {
    if (err) { return done(err); }
    if (!user) { return done(null, false); }

    // comparar passwords

    return user.comparePasswords(password, (err, isMatch) => {
      if (err) { return done(err); }
      if (!isMatch) { return done(null, false); }
      return done(null, user);
    });
  });
}));

const facebookLogin = new FacebookTokenStrategy({
  clientID: config.facebookAuth.clientID,
  clientSecret: config.facebookAuth.clientSecret,
},
((accessToken, refreshToken, profile, done) => User.upsertFbUser(
  accessToken, refreshToken, profile, (err, user) => {
    if (err) { return done(err, false); }

    if (user) {
      return done(null, user);
    }
    return done(null, false);
  },
)));

const googleLogin = new GoogleTokenStrategy({
  clientID: config.googleAuth.clientID,
  clientSecret: config.googleAuth.clientSecret,
},
((accessToken, refreshToken, profile, done) => User.upsertGoogleUser(
  accessToken, refreshToken, profile, (err, user) => {
    if (err) { return done(err, false); }

    if (user) {
      return done(null, user);
    }
    return done(null, false);
  },
)));

// todo add token expiration
const jwtLogin = new JwtStrategy(jwtOptions, ((payload, done) => User.findById(
  payload.sub, (err, user) => {
    if (err) { return done(err, false); }

    if (user) {
      return done(null, user);
    }
    return done(null, false);
  },
)));

passport.use(jwtLogin);
passport.use(localLogin);
passport.use(facebookLogin);
passport.use(googleLogin);

export const requireAuth = passport.authenticate('jwt', { session: false });
export const requireFacebook = passport.authenticate('facebook-token', { session: false });
export const requireGoogle = passport.authenticate('google-token', { session: false });
export const requireSignin = passport.authenticate('local', { session: false, failWithError: true });
