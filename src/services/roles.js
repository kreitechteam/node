export const admin = (req, res, next) => {
  if (req.user.role === 'admin') {
    next();
  } else {
    res.sendStatus(401);
  }
};

export const user = (req, res, next) => {
  if (req.user.role === 'user') {
    next();
  } else {
    res.sendStatus(401);
  }
};
