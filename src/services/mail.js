import nodemailer from 'nodemailer';
import config from '../config';

const smtpTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: config.mail.user,
    pass: config.mail.pass,
  },
});

export default smtpTransport;
