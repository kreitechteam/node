#Template for Node-Express Project.

## Demo

A running demo for the project can be seen [here](https://project-base.kreitech.io/).

## Running the project

In order to run the project, you will have to edit the config.default.js to use your configuration, and rename it to config.js.
You must set a express secret and two mongo databases, one for production and one for testing. The other configurations are optional, and are used
for facebook and google authentication, and the last one is for sending emails.

After that you just have to run npm install and npm start!

The project features live reloading thanks to nodemon package.

## ES6 Syntax

The project has been entirely coded using ES6. Translation is done using Babel.

## Database

MongoDB is using as database. The connection between mongo and node uses [Mongoose](https://mongoosejs.com/).

## Authentication
For authentication, [Passport](https://www.npmjs.com/package/passport) is used. JWT, LocalLogin, Facebook and Google
authentication strategies are configured.

## File Structure

All files related to source code are found inside the src folder. In there four folders can be found,
controllers, models, services and test. services folder contains files related to email service and passport configuration.
Routes are located in the router.js file. The main file is index.js, where Express is configured and mongodb connection established.

## Middlewares

Express is configured to use some middlewares in the index.js file. body-parser, morgan, express-paginate, express-rate-limit and helmet.

* body-parser is used to parse json requests,
* morgan is a log middleware to print requests in console.
* express-paginate allows to paginate long responses in a simple way, an example can be seen in the user controller at the get method.
* express-rate-limit and helmet are both middlewares to address some simple security issues. However, you shouldn't rely only on these to make a secure API!

## Docker

A Dockerfile and a docker-compose file can be found in the root folder. These were created with a production enviroment in mind. Dockerfile creates a container which runs the project in production mode. The compose file runs that container along with a mongoDB container.

## Testing

[Mocha](https://mochajs.org/), [Chai](https://www.chaijs.com/), [Sinon](https://sinonjs.org/) and [nyc](https://github.com/istanbuljs/nyc) are used for testing.
All files ending with .test.js are considered tests by Jest. However, there is a test folder in src/, where tests should be placed.

Use
```
npm test
```
to run tests.

### Code coverage
For code coverage run:
```
npm run test:coverage
```
Results will appear in terminal and in a folder called coverage, in the project root folder.

## eslint

Eslint with [airbnb style guide](https://github.com/airbnb/javascript) has been configured.
Some rules have been deactivated and can be seen in the configuration file, .eslintrc.

## OAuth

Facebook and Google authentication services are included.
Application ids and secrets are read from src/config.js. To use Google and Facebook authentication just add your credentials there.

To create a new Google project, refer to [Google Sign In](https://developers.google.com/identity/sign-in/web/sign-in). For Facebook, go to [Facebook Developers](https://developers.facebook.com/).

In order to sign in with Facebook in production, you have to switch the app status from development to public, in the app configuration.


## npm scripts
```
npm start
```

Starts the project in development mode. Live reloading is enabled

```
npm run lint
```
Runs eslint and outputs the warning and errors.

```
npm run lint:fix
```
Runs eslint, fixes errors and warnings that can be automatically fixed and
outputs results.

```
npm run test
```

Run tests and output results.


```
npm run build
```

Runs linter, and transpiles code using babel. Output is generated in a dist folder. Build cannot be created if eslint finds
errors that can't be automatically fixed.

```
npm run start:production
```

Starts the project in production mode, a production build must exists (```npm run build```)